---
author: Adrian Bacaliu
subtitle: Use Emacs-themes on the whole system
title: elwal
---

# Differences to `theme-magic`

This project is very much based on [theme-magic from
jcaw](https://github.com/jcaw/theme-magic). Some differences are:

- no python-script for creating json, I just make it within emacs
  - reason: when trying to support **16 colors** by editing
    `theme-magic`, sometimes it broke
- checking for `:no-enable` keyword when running automaticly after
  enabling a theme
  - reason: when enabling the `elwal-export-theme-mode` in the config,
    initializing `ef-themes` called `load-theme` once for each theme.
    Calling `pywal` and `wpg` that often (30 times) made starting very
    slow and every terminal blinked 30 times. Now only when the theme is
    actually enabled the export happens so it is save to enable the
    mode.
- customization by advising
  - extracting the colors is a bit difficult. A font-face can
    potentially not exist for some theme so fallback faces are needed.
    But sometimes this means fore- *and* background colors are needed.

# Images

<figure>
<img src="images/2024-04-09T12.21.55 97 -- screen.png" />
<figcaption>Modus-Operandi</figcaption>
</figure>

<figure>
<img src="images/2024-04-09T12.22.24 09 -- screen.png" />
<figcaption>Modus-Vivendi</figcaption>
</figure>

# Requirements

By default this will call `pywal` and `wpgtk`.

# Installation

I assume you are using the `use-package` macro and some manager like
`elpaca`.

``` elisp
(use-package elwal
  :ensure (:host gitlab :repo "Bacaliu/elwal")
  :config (elwal-export-theme-mode 1))
```

My config looks like the following:

- The first emacs-frame (which starts a server by
  `(unless (server-running-p) (server-start))`) has elwal enabled. But
  if I start a standalone emacs-process (e.g. to work with tramp or to
  debug via `--debug-init`) it has `server-process` unbound so it
  doesn't make everything flicker for no reason.
  - Placing the code in `:init` is needed when using `:defer`.
- Adding `*elwal*` to the `display-buffer-alist` ensures the
  async-buffer in which the backends are called isn't displayed.

<div class="captioned-content">

<div class="caption">

My configuration

</div>

``` elisp
(use-package elwal
  :defer
  :init
  (when server-process
    (elwal-export-theme-mode 1))
  ;; [...]
  :config
  (add-to-list 'display-buffer-alist '("*elwal*" display-buffer-no-window (nil)))
  )
```

</div>

# Configuration

I tried to have sane defaults wich work with multiple themes I use
(which are `ef-themes`, `modus-themes`, `standard-themes`, `leuven`,
`doom-themes`). If you want to change how the colors are selected you
can override the default behaviour by

1.  changing the `elwal-color-recipe` variable to a custom function
2.  by `(advice-add :override)` the default function named
    `elwal-default-color-recipe`

Eighter way, you have to make sure that your function returns an alist
in the form as below. It will be directly converted to json.

<div class="captioned-content">

<div class="caption">

setting custom colors

</div>

``` elisp
(use-package elwal
  ;; [...]
  :config
  (defun bacaliu/custom-elwal-colors ()
    ;; in general, elwal--face-fore/background can be used to get the
    ;; fore/backgroundcolor of the first existing face
    `((special
       . ((background . ,(elwal--face-background 'default))
          (foreground . ,(elwal--face-foreground 'default))
          ;; by or-ing warning-foreground is used if
          ;; cursor-background is nil
          (cursor . ,(or (elwal--face-background 'cursor)
                         (elwal--face-foreground 'warning)))))
      (colors
       . ((color0 . ,(elwal--face-background 'default))
          (color1 . ,(elwal--face-foreground 'diff-removed 'ansi-color-red))
          (color2 . ,(elwal--face-foreground 'diff-added 'ansi-color-green))
          (color3 . ,(elwal--face-foreground 'ansi-color-yellow))
          (color4 . ,(elwal--face-foreground 'ansi-color-blue))
          (color5 . ,(elwal--face-foreground 'ansi-color-magenta))
          (color6 . ,(elwal--face-foreground 'ansi-color-cyan))
          (color7 . ,(elwal--face-foreground 'shadow))
          (color8 . ,(elwal--face-background 'region 'mode-line))
          (color9 . ,(elwal--face-foreground 'ansi-color-bright-red))
          (color10 . ,(elwal--face-foreground 'ansi-color-bright-green))
          (color11 . ,(elwal--face-foreground 'ansi-color-bright-yellow))
          (color12 . ,(elwal--face-foreground 'ansi-color-bright-blue))
          (color13 . ,(elwal--face-foreground 'ansi-color-bright-magenta))
          (color14 . ,(elwal--face-foreground 'ansi-color-bright-cyan))
          (color15 . ,(elwal--face-foreground 'default))))))
  :custom
  (elwal-color-recipe #'bacaliu/custom-elwal-colors))
```

</div>
