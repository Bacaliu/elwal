;;; elwal --- make everything else using the emacs-theme

;;; Commentary:
;; elwal.el exports the current emacs-theme via pywal and wpgtk.

;;; Code:

(require 'color)
(require 'font-lock)
(require 'ansi-color)
(require 'seq)
(require 'json)
(require 'cl-lib)

(defgroup elwal nil "Group of elwal.el." :group 'faces)

(defcustom elwal-export-theme-delay 5
  "The delay for elwal to wait when startet from a hook.
Especially useful when using e.g. consult-theme which enables
themes when selected in the completion-menu."
  :type 'number
  :group 'elwal)

(defcustom elwal-json-file "~/.cache/wal/elwal.json"
  "The place to store the current theme."
  :type 'string
  :group 'elwal)

(defcustom elwal-backends '("wpg" "wal")
  "List of backends to use.  May be `wpg' or `wal', I use both.
Every program must accept the flag `--theme' followed by the path
to a json file containing the colors.  Order matters: the list is
iterated so the first backend is called before the second.

The ordered behaviour is useful, because apparrently wpg doesn't
support background and foreground-colors so calling wal after it
ensures pywalfox gets to know that a dark theme is loaded and
changes `prefers-color-scheme' to dark in firefox."
  :type '(alist)
  :group 'elwal)

(defun elwal-default-color-recipe ()
  "Default function to generate the colors.
Feel free to override this function or use an
other function by setting `elwal-color-recipe' to it."
  `((special . ((background . ,(elwal--face-background 'default))
		(foreground . ,(elwal--face-foreground 'default))
		(cursor . ,(or (elwal--face-background 'cursor)
			       (elwal--face-foreground 'warning)))))
    (colors . ((color0 . ,(elwal--face-background 'default))
	       (color1 . ,(elwal--face-foreground 'diff-removed 'ansi-color-red))
	       (color2 . ,(elwal--face-foreground 'diff-added 'ansi-color-green))
	       (color3 . ,(elwal--face-foreground 'ansi-color-yellow))
	       (color4 . ,(elwal--face-foreground 'ansi-color-blue))
	       (color5 . ,(elwal--face-foreground 'ansi-color-magenta))
	       (color6 . ,(elwal--face-foreground 'ansi-color-cyan))
	       (color7 . ,(elwal--face-foreground 'shadow))
	       (color8 . ,(elwal--face-background 'hl-line 'region 'mode-line))
	       (color9 . ,(elwal--face-foreground 'ansi-color-bright-red))
	       (color10 . ,(elwal--face-foreground 'ansi-color-bright-green))
	       (color11 . ,(elwal--face-foreground 'ansi-color-bright-yellow))
	       (color12 . ,(elwal--face-foreground 'ansi-color-bright-blue))
	       (color13 . ,(elwal--face-foreground 'ansi-color-bright-magenta))
	       (color14 . ,(elwal--face-foreground 'ansi-color-bright-cyan))
	       (color15 . ,(elwal--face-foreground 'default))))))

(defcustom elwal-color-recipe
  'elwal-default-color-recipe
  "This is the function called by `elwal-from-emacs'.
It must return a nested alist (see the default function
`elwal-default-color-recipe' as a reference)"
  :type 'function
  :group 'elwal)

(defun elwal--color-name-to-hex (color-name)
  "Convert a `COLOR-NAME' into a 6-digit hex value.

E.g. \"Orange\" -> \"#FFA500\".

Note that this conversion method IS LOSSY. If you supply a hex
name as the color-name, it may spit out a slightly different hex
value due to rounding errors."
  (if color-name
      ;; Upcase result to make it neat.
      (upcase
       ;; Have to convert to rgb first, *then* convert back to hex.
       (apply
        'color-rgb-to-hex
        (append (color-name-to-rgb
		 color-name)
                ;; We have to specify "2" as the fourth argument
                '(2))))
    nil))

(defun elwal--face-background (&rest faces)
  "Return the first extracteable color of given `FACES' as hex.
Try every face of `FACES' after each other.  If nothing worked try
again but with enabled face-inheritance."
  (catch 'succ
    (dolist (face faces)
      (when (and (facep face) (face-background face))
	(throw 'succ (elwal--color-name-to-hex (face-background face)))))
    (dolist (face faces)
      (when (and (facep face) (face-background face nil t))
	(throw 'succ (elwal--color-name-to-hex (face-background face nil t)))))
    ))

(defun elwal--face-foreground (&rest faces)
  "Return the first extracteable color of given `FACES' as hex.
Try every face of `FACES' after each other.  If nothing worked try
again but with enabled face-inheritance."
  (catch 'succ
    (dolist (face faces)
      (when (and (facep face) (face-foreground face))
	(throw 'succ (elwal--color-name-to-hex (face-foreground face)))))
    (dolist (face faces)
      (when (and (facep face) (face-foreground face nil t))
	(throw 'succ (elwal--color-name-to-hex (face-foreground face nil t)))))
    ))

;;;###autoload
(defun elwal-from-emacs ()
  "Apply the current theme to everything via wpgtk and pywal."
  (interactive)
  (with-temp-file elwal-json-file
    (insert (json-encode (funcall elwal-color-recipe)))
    (json-pretty-print-buffer))

  (start-process
   "elwal-process" "*elwal*"
   "sh" "-c"
   (mapconcat
    (lambda (backend)
      (format "%s --theme %s"
	      backend
	      (expand-file-name elwal-json-file)))
    elwal-backends "; ")))


(defun elwal-from-emacs--wrapper (&rest args)
  "Wrapper for `elwal-from-emacs' to be used as advice.
Check for the `:no-enable'-keyword in `ARGS' to prevent useless exporting
on startup."
  (unless (plist-member args :no-enable)
    (cancel-function-timers #'elwal-from-emacs) ;; prevent retriggering
    (if (eq elwal-export-theme-delay 0)
	(elwal-from-emacs)
      (run-with-idle-timer elwal-export-theme-delay nil #'elwal-from-emacs))))

;;;###autoload
(define-minor-mode elwal-export-theme-mode
  "Automatically export the Emacs theme.

If this mode is active, the Linux theme will be updated
automatically when you change the Emacs theme.

Under the hood, this mode calls `elwal-from-emacs' when you
change the theme.  See `elwal-from-emacs' for more
information."
  :global t
  :lighter " EW"
  :group 'elwal
  :after-hook
  (if elwal-export-theme-mode
      ;; Was disabled. We should now enable.
      (progn
	(advice-add 'enable-theme :after #'elwal-from-emacs--wrapper)
	(advice-add 'load-theme :after #'elwal-from-emacs--wrapper))
    
    ;; Was enabled. We should now disable.
    (progn
      (advice-remove 'enable-theme #'elwal-from-emacs--wrapper)
      (advice-remove 'load-theme #'elwal-from-emacs--wrapper))))

(provide 'elwal)
;;; elwal.el ends here
